import ReactDOM from "react-dom/client";
import React from "react";

import "./styles/index.css";
import App from "./router/routes";

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
