import axiosWithConfig from "../axiosWithConfig";

export const getIdeas = async (params) => {
  try {
    let query = "";

    if (params) {
      const queryParams = [];

      let key;
      for (key in params) {
        queryParams.push(`${key}=${params[key]}`);
      }

      query = queryParams.join("&");
    }
    const url = query ? `/api/ideas?${query}` : "/api/ideas";
    const response = await axiosWithConfig.get(url);
    console.log("API Response:", response.data);

    return response.data;
  } catch (error) {
    throw Error(error.response.data.message);
  }
};

export const getDetailIdeas = async (id) => {
  try {
    const response = await axiosWithConfig.get(`/api/ideas/${id}`);

    return response.data;
  } catch (error) {
    throw Error(error.response.data.message);
  }
};
