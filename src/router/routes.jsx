import { createBrowserRouter, RouterProvider } from "react-router-dom";

import Index from "@/pages/ideas";

export default function routes() {
  const router = createBrowserRouter([
    {
      path: "/",
      element: <Index />,
    },
    {
      path: "*",
      element: <div>404 page not found</div>,
    },
  ]);

  return <RouterProvider router={router} />;
}
