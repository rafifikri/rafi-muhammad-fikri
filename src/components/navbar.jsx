import React from "react";
import Logo from "../assets/suitmedia.png";

export default function navbar() {
  return (
    <nav className="w-full h-20 flex items-center justify-between px-40 bg-orange-500 text-white">
      <div className="flex items-center">
        <img src={Logo} className="h-12 w-30" alt="LOGO" />
      </div>
      <div className="flex space-x-6 items-center">
        <button className="text-lg hover:underline font-semibold">Work</button>
        <button className="text-lg hover:underline font-semibold">About</button>
        <button className="text-lg hover:underline font-semibold">
          Services
        </button>
        <button className="text-lg hover:underline font-semibold">Ideas</button>
        <button className="text-lg hover:underline font-semibold">
          Careers
        </button>
        <button className="text-lg hover:underline font-semibold">
          Contact
        </button>
      </div>
    </nav>
  );
}
