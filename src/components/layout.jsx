import Navbar from "../components/navbar";
import Header from "../components/header";

export default function Layout({ children }) {
  return (
    <div className="w-full relative">
      <div className="w-full fixed z-50">
        <Navbar />
      </div>
      <div className="mt-20 mb-32 relative z-10">
        <Header />
      </div>
      <div className="px-40">{children}</div>
    </div>
  );
}
