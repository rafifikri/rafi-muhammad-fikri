import React from "react";
import Background from "../assets/lamps.jpg";

export default function Header() {
  return (
    <div className="relative">
      <img
        src={Background}
        alt=""
        className="w-full h-[40rem] object-cover clip-right-height"
      />
      <div className="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 text-white text-center">
        <h1 className="text-6xl">Ideas</h1>
        <p className="text-2xl">Where all our great things begin</p>
      </div>
    </div>
  );
}
