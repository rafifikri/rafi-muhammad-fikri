import { useEffect, useState } from "react";
import { useSearchParams } from "react-router-dom";
import LazyLoad from "react-lazy-load";

import { getIdeas } from "@/utils/apis/ideas/api";
import { Select } from "@/components/select";
import Layout from "@/components/layout";
import Work from "@/assets/work.jpg";
import Pagination from "@/components/pagination";

export default function Index() {
  const [ideas, setIdeas] = useState([]);
  const [searchParams, setSearchParams] = useSearchParams();
  const [meta, setMeta] = useState();
  const [itemsPerPage, setItemsPerPage] = useState([]);

  useEffect(() => {
    fetchData();
  }, [searchParams, itemsPerPage]);

  async function fetchData() {
    let query = {};
    for (const entry of searchParams.entries()) {
      query[entry[0]] = entry[1];
    }

    try {
      const result = await getIdeas({ ...query });
      const { ...rest } = result.meta;
      setIdeas(result.data);
      setMeta(rest);
    } catch (error) {
      console.error(error.toString());
    }
  }

  const formatDate = (dateString) => {
    const options = { day: "2-digit", month: "long", year: "numeric" };
    return new Date(dateString).toLocaleDateString("id-ID", options);
  };

  function handlePrevNextPage(page) {
    const newSearchParams = new URLSearchParams(searchParams.toString());
    newSearchParams.set("page[number]", String(page));
    setSearchParams(newSearchParams);
  }

  function handleSortChange(value) {
    const sortOptions = {
      Newest: "-published_at",
      Oldest: "published_at",
    };

    const selectedSort = sortOptions[value];

    const newSearchParams = new URLSearchParams(searchParams?.toString());
    newSearchParams.set("sort", selectedSort);
    setSearchParams(newSearchParams);
  }

  function handleShowPage(value) {
    const newSearchParams = new URLSearchParams(searchParams.toString());
    newSearchParams.set("page[size]", value);
    setSearchParams(newSearchParams);
    setItemsPerPage(value);
  }

  let currentPage = meta?.from;
  let lastPage = meta?.to;
  let totalPages = meta?.total;

  return (
    <Layout>
      <div className="flex justify-between items-center mb-4">
        <p>
          Showing {currentPage}-{lastPage} of {totalPages}
        </p>
        <div className="flex gap-4">
          <div className="flex items-center gap-4">
            <p>Show per page:</p>
            <Select
              name="page[size]"
              options={["10", "20", "50"]}
              onChange={(e) => handleShowPage(e.target.value)}
            />
          </div>

          <div className="flex items-center gap-4">
            <p>Sort by:</p>
            <Select
              name="sort"
              options={["Newest", "Oldest"]}
              onChange={(e) => handleSortChange(e.target.value)}
            />
          </div>
        </div>
      </div>

      <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-4 lg:grid-cols-4 gap-8">
        {ideas.map((data) => (
          <div key={data.id} className="shadow-md rounded-xl hover:scale-105">
            <LazyLoad height={220}>
              <img
                src={Work}
                alt="image"
                className="h-52 w-full mb-3 rounded-t object-cover"
              />
            </LazyLoad>
            <p className="pb-1 px-5 font-medium text-gray-400">
              {formatDate(data.published_at)}
            </p>
            <h1 className="px-5 font-semibold text-xl overflow-hidden text-ellipsis h-[90px]">
              {data.title}
            </h1>
          </div>
        ))}
      </div>
      <div className="flex justify-center mt-5">
        <Pagination
          meta={meta}
          onClickPrevious={() => handlePrevNextPage(meta?.current_page - 1)}
          onClickNext={() => handlePrevNextPage(meta?.current_page + 1)}
          onClickPage={(page) => handlePrevNextPage(page)}
        />
      </div>
    </Layout>
  );
}
